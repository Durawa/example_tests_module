__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2019 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__name__ = "example_functions_tests"
__version__ = '1.0'

from io import StringIO
import sys
import unittest
from unittest.mock import patch, Mock, mock_open

if "__main__" != __name__:
    sys.path.append("")

from example_tests_module.example_functions import *


class ExampleFunctionsTests(unittest.TestCase):
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_empty_string(self, mock_stdout):
        print_var("")
        self.assertEqual(mock_stdout.getvalue(), "\n")

    @patch('builtins.print', new_callable=Mock())
    def test_print_empty_string_multiple_times(self, mock_stdout):
        print_var("", 10)
        mock_stdout.assert_called_once_with("")

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_empty_string_without_endline(self, mock_stdout):
        print_var("", end="")
        self.assertEqual(mock_stdout.getvalue(), "")

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_string(self, mock_stdout):
        test_string = "This string"
        print_var(test_string, end="")
        self.assertEqual(mock_stdout.getvalue(), test_string)

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_string_five_times(self, mock_stdout):
        test_string = "O"
        print_var(test_string, 5, end="")
        self.assertEqual(mock_stdout.getvalue(), test_string*5)

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_number(self, mock_stdout):
        test_string = "7"
        print_var(test_string)
        self.assertEqual(mock_stdout.getvalue(), "{}\n".format(test_string))

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_number_few_times(self, mock_stdout):
        test_string = "7"
        print_var(test_string, 7)
        self.assertEqual(mock_stdout.getvalue(), "{}\n".format(test_string*7))

    @patch('builtins.input', return_value="")
    def test_ask_user_ang_got_nothing(self, mock_stdin):
        result = ask_user()
        self.assertEqual(result, "")

    @patch('builtins.input', return_value="Something")
    def test_ask_user_ang_got_something(self, mock_stdin):
        result = ask_user()
        self.assertEqual(result, "Something")

    @patch('builtins.open', mock_open(read_data=""))
    def test_read_file_when_is_empty(self):
        text = read_file("some_file.txt")
        self.assertEqual(text, "")

    @patch('builtins.open', mock_open(read_data="My Line"))
    def test_read_file_when_there_is_one_line(self):
        text = read_file("some_file.txt")
        self.assertEqual(text, "My Line")

    @patch('builtins.open', mock_open(read_data="My Line 1\nMy Line 3\nMy Line 666"))
    def test_read_file_when_there_is_three_lines(self):
        text = read_file("some_file.txt")
        self.assertEqual(text, "My Line 1\nMy Line 3\nMy Line 666")

    def test_read_file_when_there_is_three_empty_lines(self):
        filename = "BIG_FILE_NAME.bin"
        with patch('builtins.open', mock_open(read_data="\n\n")) as mock_file:
            text = read_file(filename)
        self.assertEqual(text, "\n\n")
        mock_file.assert_called_with(filename, "r")

    @staticmethod
    def test_write_file_with_empty_string():
        filename = "BIG_FILE_NAME.bin"
        with patch('builtins.open', mock_open()) as mock_file:
            write_file(filename, "")
        mock_file.assert_called_once_with(filename, "w+")
        mock_file().write.assert_called_once_with("")

    @staticmethod
    def test_write_file_with_one_line():
        filename = "BIG_FILE_NAME.bin"
        text = "Python is the best for quick job"
        with patch('builtins.open', mock_open()) as mock_file:
            write_file(filename, text)
        mock_file.assert_called_once_with(filename, "w+")
        mock_file().write.assert_called_once_with(text)

    @staticmethod
    @patch('builtins.open', new_callable=mock_open)
    def test_write_file_with_one_line(mock_file):
        filename = "BIG_FILE_NAME.bin"
        text = "Python is the best for quick job"
        write_file(filename, text)
        mock_file.assert_called_once_with(filename, "w+")
        mock_file().write.assert_called_once_with(text)

    @staticmethod
    @patch('builtins.open', new_callable=mock_open)
    def test_write_file_with_empty_line_on_the_end(mock_file):
        filename = "BIG_FILE_NAME.bin"
        text = "Python is the best for quick job\nIt is quick and simple language\n"
        write_file(filename, text)
        mock_file.assert_called_once_with(filename, "w+")
        mock_file().write.assert_called_once_with(text)


if "__main__" == __name__:
    unittest.main()
